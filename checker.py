#!/usr/bin/env python3
__author__ = "Tiago Espinha"
__version__ = "0.1.0"
__license__ = "MIT"

from datetime import datetime
from smtplib import SMTP
from urllib import request, parse
import ssl
import imaplib
import email
import email.header
import uuid
import time
import sys

context = ssl.create_default_context()

ERROR_READING_EMAILS = -1
ERROR_EMAIL_NOT_FOUND = -2
SUCCESS = 1

RETRIES = 5

def test_email_is_present(inbox_connection, random_hash, retries):
    time.sleep(5)

    rv, data = inbox_connection.select("INBOX")
    rv, data = inbox_connection.search(None, "ALL")

    # If we have more than one e-mail, something fishy is going on.
    # We should probably alert

    return_value = None
    for num in data[0].split():
        rv, data = inbox_connection.fetch(num, '(RFC822)')
        if rv != 'OK':
            print("ERROR getting message", num)
            return_value = { "return_code": ERROR_READING_EMAILS }
            break

        msg = email.message_from_bytes(data[0][1])
        decode = email.header.decode_header(msg['Subject'])[0]
        subject = decode[0]

        if random_hash in subject:
            return_value = { "return_code": SUCCESS, "email_ref": num }
            break

    if return_value is not None and return_value["return_code"] == SUCCESS:
        return return_value
    elif return_value is not None and retries > 0:
        return test_email_is_present(inbox_connection, random_hash, retries-1)
    elif return_value is not None:
        return return_value
    elif retries > 0:
        return test_email_is_present(inbox_connection, random_hash, retries-1)
    else:
        return { "return_code": ERROR_EMAIL_NOT_FOUND }

def delete_test_email(inbox_connection, email_ref):
    inbox_connection.store(email_ref, '+FLAGS', '\\Deleted')
    inbox_connection.expunge()

def mock_notify(message):
    print("Would have notified "+ message)

def notify_failure(api_key, message, recipient_number):
    #return mock_notify(message)
    data = parse.urlencode({"recipients": recipient_number,
                            "body": message,
                            "language": "en-gb",
                            "voice": "male",
                            "repeat": "2"}).encode()

    req =  request.Request(
        "https://rest.messagebird.com/voicemessages",
        headers={"Authorization": "AccessKey " + api_key},
        data=data)
    resp = request.urlopen(req)
    return None

def setup_inbox_connection(email_server, email_address, email_password):
    M = imaplib.IMAP4_SSL(email_server)
    rv, data = M.login(email_address, email_password)
    rv, data = M.select("INBOX")
    rv, data = M.search(None, "ALL")
    return M

def send_test_email(email_server, email_address, email_password, random_uuid):
    test_message = message = """\
From: %s
To: %s
Date: %s
Message-ID: <%s>
Subject: %s

This is a test e-mail."""

    dt = datetime.now()
    dt_str = dt.strftime("%a, %d %b %Y %H:%M:%S +0000")
    message_id = "%.20f" % time.time()
    message_id += "@espinha.co.uk"
    with SMTP(email_server, 587) as smtp:
        smtp.ehlo()
        smtp.starttls(context=context)
        smtp.login(email_address, email_password)
        smtp.sendmail(email_address, email_address, test_message %(email_address, email_address, dt_str, message_id, random_uuid))

def main(email_server, email_address, email_password, api_key, recipient):
    try:
        random_uuid = uuid.uuid4().hex
        M = setup_inbox_connection(email_server, email_address, email_password)
        send_test_email(email_server, email_address, email_password, random_uuid)

        email_is_present = test_email_is_present(M, random_uuid, retries=RETRIES)
        if email_is_present["return_code"] == SUCCESS:
            delete_test_email(M, email_is_present["email_ref"])
        else:
            notify_failure(api_key=api_key, message="Failure to send and receive e-mail. The e-mail server is possibly down.", recipient_number=recipient)
            return -1
    except:
        notify_failure(api_key=api_key, message="Exception when trying to send or receive e-mails.", recipient_number=recipient)
        return -1

    return 0

if __name__ == "__main__":
    if len(sys.argv) != 6:
        print("Error. Supply email server, address, password, api key and recipient.")
        sys.exit(-1)

    sys.exit(main(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5]))
