#!/bin/bash

if [[ ! -v ESPINHA_CO_UK_KEY || ! -v ESPINHA_PT_KEY || ! -v ESPINHA_NET_KEY || ! -v ESPINHAS_NET_KEY || ! -v ESPINHA_NL_KEY ]]; then
    echo "Please set env vars ESPINHA_CO_UK_KEY,  ESPINHA_PT_KEY, ESPINHA_NET_KEY, ESPINHAS_NET_KEY, ESPINHA_NL_KEY"
    exit -1
fi

declare -A domain_keys   
domain_keys=( ['espinha.co.uk']="$ESPINHA_CO_UK_KEY"
              ['espinha.pt']="$ESPINHA_PT_KEY"
              ['espinha.net']="$ESPINHA_NET_KEY"
              ['espinhas.net']="$ESPINHAS_NET_KEY"
              ['espinha.nl']="$ESPINHA_NL_KEY"
            )

for domain in espinha.co.uk espinha.pt espinha.net espinhas.net espinha.nl
do
    for host in @ autoconfig autoconfig.box autodiscover autodiscover.box box www
    do
        echo "$host and $domain"
	if [ "$NAMECHEAP" = true ]; then
		echo 'namecheap'
		curl https://dynamicdns.park-your-domain.com/update\?host=$host\&domain=$domain\&password=${domain_keys[$domain]}\&ip=xxxxxxxxx
	else
 		echo 'dnshe'
		if [ "$host" = "@" ]; then
			hostpart=""
		else
			hostpart="$host."
		fi
		echo "https://$hostpart$domain:${domain_keys[$domain]}@dyn.dns.he.net/nic/update?hostname=$hostpart$domain&myip=192.168.0.1"
	fi		
        echo "\n"
    done
done
